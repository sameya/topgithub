package com.example.topgithub.view.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.topgithub.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class )
class   MainActivityTest
{
    @Test
    fun testMainActivity() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)   //creates an activity
        onView(withId(R.id.main)).check(matches(isDisplayed()))
    }

    @Test
    fun testMainActivityComponents() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.logoImageView)).check(matches(isDisplayed()))
        onView(withId(R.id.spinner)).check(matches(isDisplayed()))
        onView(withId(R.id.languageTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.button)).check(matches(isDisplayed()))
        onView(withId(R.id.languageTextView)).check(matches(withText(R.string.language)))
    }

    @Test
    fun testButton() {
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.button)).perform( click())
        onView(withId(R.id.repository)).check(matches(isDisplayed()))
    }
}

