package com.example.topgithub.view.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.topgithub.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class RepositoriesActivityTest
{
    @get: Rule
    val activityRule : ActivityScenarioRule<RepositoriesActivity> = ActivityScenarioRule(RepositoriesActivity::class.java)

    @Test
    fun testRepositoriesActivity() {
        onView(withId(R.id.repository)).check(matches(isDisplayed()))
    }

    @Test
    fun testRepositoriesComponents()
    {
        onView(withId(R.id.recycleView)).check(matches(isDisplayed()))
    }

    @Test
    fun testItemClick()
    {

    }
}