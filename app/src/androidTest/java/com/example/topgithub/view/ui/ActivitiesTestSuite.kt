package com.example.topgithub.view.ui

import org.junit.runner.RunWith
import org.junit.runners.Suite


@RunWith(Suite::class)
@Suite.SuiteClasses(
    MainActivityTest::class,
    RepositoriesActivityTest::class,
    RepoDetailsActivityTest::class
)
class ActivitiesTestSuite {
}