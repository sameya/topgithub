package com.example.topgithub.data.database

import com.example.topgithub.model.GitHubUsersItem
import com.example.topgithub.model.Repo

//data entity to model (db to api)
fun DataEntity.toUsers() = GitHubUsersItem(
    this.avatar,
    this.name,
    Repo("repoDesc", this.repo, "repoUrl"),
    this.url,
    this.username
)

fun List<DataEntity>.toDataList() = this.map { it.toUsers() }


//model to data entity (api to db)
fun GitHubUsersItem.toDataEntity() = DataEntity(
    username = this.username,
    name = this.name,
    url = this.url,
    avatar = this.avatar,
    repo = this.repo.name
)

fun List<GitHubUsersItem>.toDataEntityList() = this.map { it.toDataEntity() }
