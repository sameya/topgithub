package com.example.topgithub.data.network

import com.example.topgithub.model.GitHubUsers
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {

    /*Flowable -> Emits 0 or n items and terminates with success/error event.
    Supports backpressure, which allows to control how fast a source emits items.
     */

    @GET("developers?since=weekly")
    fun getAllRepos(@Query("language") language: String): Flowable<GitHubUsers>
}