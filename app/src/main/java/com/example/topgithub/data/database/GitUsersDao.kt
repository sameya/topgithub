package com.example.topgithub.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

//insertion and query db
@Dao
interface GitUsersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: List<DataEntity>)

    @Query("SELECT * from trendingRepositories")
    fun queryData(): Single<List<DataEntity>>

}