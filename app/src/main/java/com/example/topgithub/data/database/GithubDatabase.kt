package com.example.topgithub.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.topgithub.helper.utilities.DATABASE_NAME

@Database(entities = [DataEntity::class], version = 1)
abstract class GithubDatabase : RoomDatabase() {

    abstract fun gitUsersDao(): GitUsersDao

    //instantiate db for Room
    companion object {

        @Volatile
        private var instance: GithubDatabase? = null

        //thread locking
        private val LOCK = Any()

        //db builder
        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                GithubDatabase::class.java,
                DATABASE_NAME
            ).fallbackToDestructiveMigration().build()

        //creating instance of db
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }
    }
}