package com.example.topgithub.data

import com.example.topgithub.data.network.ApiService
import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitBuilder {
    private const val URL ="https://github-trending-api.now.sh/"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create(Gson())) // Add your adapter factory to handler Errors
            .build()
    }

    val apiService: ApiService = getRetrofit()
        .create(
        ApiService::class.java)
}