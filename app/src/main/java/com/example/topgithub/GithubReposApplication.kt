package com.example.topgithub

import android.app.Application
import com.example.topgithub.data.database.GithubDatabase

class GithubReposApplication :Application() {
    companion object {
        lateinit var instance: GithubReposApplication
        lateinit var database: GithubDatabase
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        database = GithubDatabase.invoke(this)
    }
}