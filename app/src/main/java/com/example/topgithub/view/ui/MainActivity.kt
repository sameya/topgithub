package com.example.topgithub.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.topgithub.R
import com.example.topgithub.helper.utilities.LANGUAGE

class MainActivity : AppCompatActivity() {

    companion object{
        var language : String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        goToNext()
    }

    private fun goToNext() {

        val spinner: Spinner = findViewById(R.id.spinner)
        val languages = resources.getStringArray(R.array.languages)

        val arrayAdapter: ArrayAdapter<String> = ArrayAdapter(
            this, R.layout.support_simple_spinner_dropdown_item,
            languages
        )
        spinner.adapter = arrayAdapter
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                Toast.makeText(
                    this@MainActivity,
                    "Selected language is: ${languages[position]}", Toast.LENGTH_SHORT
                ).show()
                language = languages[position].toLowerCase()
                val intent = Intent(this@MainActivity, RepositoriesActivity::class.java)
                intent.putExtra(LANGUAGE, language)
                val button: Button = findViewById(R.id.button)
                button.setOnClickListener {
                    startActivity(intent)
                }
            }
        }
    }
}