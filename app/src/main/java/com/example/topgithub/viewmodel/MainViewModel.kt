package com.example.topgithub.viewmodel

import androidx.lifecycle.ViewModel
import com.example.topgithub.di.DaggerAppComponent
import com.example.topgithub.repository.MainRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainViewModel : ViewModel() {

    @Inject
    lateinit var mainRepository : MainRepository

    private val compositeDisposable by lazy { CompositeDisposable() }

    init {
        DaggerAppComponent.create().inject(this)
        compositeDisposable.add(mainRepository.fetchDataFromDatabase())
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}