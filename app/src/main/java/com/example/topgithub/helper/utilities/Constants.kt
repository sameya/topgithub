package com.example.topgithub.helper.utilities

const val BASE_URL ="https://github-trending-api.now.sh/"
const val DATABASE_NAME = "githubrepos.db"
const val LANGUAGE = "language"
const val USERNAME = "username"
const val URL = "url"
const val NAME = "name"
const val REPO_NAME ="repo_name"
const val REPO_URL = "repo_url"
const val REPO_DESC = "repo_desc"
const val AVATAR = "avatar"

