package com.example.topgithub.di

import com.example.topgithub.data.network.GithubApi
import com.example.topgithub.data.network.GithubApiService
import com.example.topgithub.model.GitHubUsers
import com.example.topgithub.model.GitHubUsersItem
import com.example.topgithub.repository.MainRepository
import com.example.topgithub.view.adapter.ClickListener
import com.example.topgithub.view.adapter.MainAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideGitHubApi() : GithubApi = GithubApiService.getClient()

    @Provides
    fun provideRepository() = MainRepository()

}