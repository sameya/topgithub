package com.example.topgithub.di

import com.example.topgithub.repository.MainRepository
import com.example.topgithub.view.ui.RepositoriesActivity
import com.example.topgithub.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(mainRepository: MainRepository)

    fun inject(viewModel: MainViewModel)

    fun inject(repositoriesActivity: RepositoriesActivity)
}