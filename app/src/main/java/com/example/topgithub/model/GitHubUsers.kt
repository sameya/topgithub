package com.example.topgithub.model

class GitHubUsers : ArrayList<GitHubUsersItem>()

data class GitHubUsersItem(
    val avatar: String,
    val name: String,
    val repo: Repo,
    val url: String,
    val username: String
)

data class Repo(
    val description: String,
    val name: String,
    val url: String
)

//data class dummy(
//    val `data`: List<Data>
//)
//
//data class Data(
//    val images: Images,
//    val title: String,
//    val type: String,
//    val username: String
//)
//
//data class Images(
//    val fixed_height_small_still: FixedHeightSmallStill
//)
//
//data class FixedHeightSmallStill(
//    val height: String,
//    val size: String,
//    val url: String,
//    val width: String
//)
